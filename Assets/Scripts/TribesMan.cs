﻿using UnityEngine;
using System.Collections;

public class TribesMan : MonoBehaviour
{
    public enum TribesManState
    {
        Idle,
        Chant,
        Sway,
        Die
    }
    private static int STATE_KEY = Animator.StringToHash("State");

    private Animator _animator;
    private TribesManState _currentState;

    public TribesManState CurrentState
    {
        get { return _currentState; }
    }

    void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _animator.speed = Random.Range(0.5f, 1.25f);
    }

    // Use this for initialization
    public void SetState(TribesManState state)
    {
        if (CurrentState == TribesManState.Die) return;
        _currentState = state;
        _animator.SetInteger(STATE_KEY, (int)state);
        if (state == TribesManState.Die) Destroy(gameObject, 2);
    }
}
