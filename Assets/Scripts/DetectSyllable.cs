﻿using System;
using UnityEngine;

public class DetectSyllable : MonoBehaviour
{
    /// <summary>
    /// Detection tolerance
    /// </summary>
    public int tolerance = 50;

    public int decibelTreshold;

    /// <summary>
    /// Short syllable max lenght
    /// </summary>
    public float shortSyllableMax = 0.1f;
    /// <summary>
    /// Max distance limit for snapping
    /// </summary>
    public float maxSnapDistance;
    /// <summary>
    /// Snap to closest
    /// </summary>
    public bool snapToClosest;
    /// <summary>
    /// Known syllables
    /// </summary>
    public Syllable[] syllables;

    public Action<Syllable> OnShortSyllable;
    public Action<Syllable> OnLongSyllableBegan;
    public Action<Syllable> OnLongSyllableChanged;
    public Action<Syllable> OnLongSyllableEnded;

    private float _syllableStartTime = 0;
    private bool _isStartEventFired = false;

    private Syllable _startSyllable;
    private Syllable _currentSyllable;

    public bool log;

    public bool disableMic = false;

    private int ticks;
    private float cumFreq;
    private float cumTime;

    public float detectionInterval = 0.1f;

    private ChantManager cm;

    // Use this for initialization
    void Start()
    {
        if (detectionInterval > shortSyllableMax) Debug.LogError("detectionInterval > shortSyllableMax");
        if (maxSnapDistance > 0 && maxSnapDistance < tolerance) Debug.LogError("maxSnapDistance < tolerance");
        cm = FindObjectOfType<ChantManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!cm.IsCalibrationFinished()) return;
        if (MicHandle.Instance)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (OnShortSyllable != null) OnShortSyllable(syllables[0]);
                Reset();
                _isStartEventFired = false;
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                if (OnShortSyllable != null) OnShortSyllable(syllables[1]);
                Reset();
                _isStartEventFired = false;
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                if (OnShortSyllable != null) OnShortSyllable(syllables[2]);
                Reset();
                _isStartEventFired = false;
            }

            if (disableMic) return;

            if (MicHandle.Instance.Decibel[0] > decibelTreshold)
            {
                ticks++;
                cumFreq += MicHandle.Instance.Pitch[0];
                cumTime += Time.deltaTime;


                if (cumTime > detectionInterval)
                {
                    GetCurrentSyllable();
                }

                if (_syllableStartTime == 0)
                {
                    if (_currentSyllable != null && _startSyllable == null)
                    {
                        _syllableStartTime = Time.realtimeSinceStartup;
                        _startSyllable = _currentSyllable;
                    }
                }

                if (_startSyllable != null && TimeSinceStart > shortSyllableMax)
                {
                    if (!_isStartEventFired)
                    {
                        _isStartEventFired = true;
                        if(log) Debug.Log("OnLongSyllableBegan: " + _startSyllable);
                        if (OnLongSyllableBegan != null) OnLongSyllableBegan(_startSyllable);
                    }
                }
            }
            else if (_startSyllable != null)
            {
                if (!_isStartEventFired)
                {
                    if (log) Debug.Log("OnShortSyllable: " + _startSyllable);
                    if (OnShortSyllable != null) OnShortSyllable(_startSyllable);
                }
                else
                {
                    if (log) Debug.Log("OnLongSyllableEnded: " + _currentSyllable);
                    if (OnLongSyllableEnded != null) OnLongSyllableEnded(_currentSyllable);
                }
                Reset();
            }
            else
            {
                GetCurrentSyllable();
                if (_currentSyllable != null)
                {
                    if (log) Debug.Log("OnShortSyllable: " + _currentSyllable);
                    if (OnShortSyllable != null) OnShortSyllable(_currentSyllable);
                    Reset();
                }
                _isStartEventFired = false;
            }
        }
    }

    private void Reset()
    {
        _isStartEventFired = false;
        _syllableStartTime = 0;
        ticks = 0;
        cumFreq = 0;
        cumTime = 0;
        _startSyllable = _currentSyllable = null;
    }

    private void GetCurrentSyllable()
    {
        if (ticks > 0 && cumFreq > 0)
        {
            Syllable previousSyllable = _currentSyllable;
            _currentSyllable = GetSyllable(cumFreq / ticks, snapToClosest);
            if (_isStartEventFired)
            {
                if (_currentSyllable != previousSyllable)
                {
                    Debug.Log("OnLongSyllableChanged: " + _currentSyllable);
                    if (OnLongSyllableChanged != null)
                    {
                        OnLongSyllableChanged(_currentSyllable);
                    }
                }
            }
        }
        ticks = 0;
        cumFreq = 0;
        cumTime = 0;
    }

    private float TimeSinceStart
    {
        get { return Time.realtimeSinceStartup - _syllableStartTime; }
    }

    private Syllable GetSyllable(float pitch, bool snap)
    {
        Syllable closest = syllables[0];
        float minDist = Mathf.Abs(pitch - closest.syllable);
        for (int i = 1; i < syllables.Length; i++)
        {
            float newDist = Mathf.Abs(pitch - syllables[i].syllable);
            if (newDist < minDist)
            {
                minDist = newDist;
                closest = syllables[i];
            }
        }

        if (!snap)
        {
            if (closest != null && pitch > closest.syllable - tolerance && pitch < closest.syllable + tolerance)
            {
                return closest;
            }
            else return null;
        }
        else if (maxSnapDistance <= 0 || (pitch > closest.syllable - maxSnapDistance && pitch < closest.syllable + maxSnapDistance))
        {
            return closest;
        }
        else return null;
    }

    private Syllable GetSyllable(bool snap)
    {
        return GetSyllable(MicHandle.Instance.Pitch[0], snap);
    }

    [Serializable]
    public class Syllable
    {
        public string name;
        public int syllable;
        public ChantManager.ChantType chantType;

        public override string ToString()
        {
            return name + " - " + syllable;
        }
    }
}
