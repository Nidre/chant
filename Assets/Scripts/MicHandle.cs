﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

public class MicHandle : MonoBehaviour
{
    private static MicHandle instance;

    private const int FREQUENCY = 48000;    // Wavelength, I think.
    public int samplecount = 1024;   // Sample Count.
    private const float REFVALUE = 0.1f;    // RMS value for 0 dB.
    public float threshold = 0.02f;  // Minimum amplitude to extract pitch (recieve anything)

    public int clamp = 160;            // Used to clamp dB (I don't really understand this either).

    private float rmsValue;            // Volume in RMS
    private float dbValue;             // Volume in DB
    private float pitchValue;          // Pitch - Hz (is this frequency?)
    private int blowingTime;           // How long each blow has lasted

    private float lowPassResults;      // Low Pass Filter result
    private float peakPowerForChannel; //

    private float[] samples;           // Samples
    private float[] spectrum;          // Spectrum

    public float sensivity = 10000;

    private AudioSource _audioSource;

    public float[] Pitch
    {
        get { return pitch; }
    }

    public float[] Decibel
    {
        get { return decibel; }
    }

    public static MicHandle Instance
    {
        get { return instance; }
    }

    public void Start()
    {
        samples = new float[samplecount];
        spectrum = new float[samplecount];

        _audioSource = GetComponent<AudioSource>();

        StartMicListener();
    }

    public void Update()
    {
        instance = this;
        //// If the audio has stopped playing, this will restart the mic play the clip.
        //if (!_audioSource.isPlaying)
        //{
        //    StartMicListener();
        //}

        // Gets volume and pitch values
        AnalyzeSound();
    }

    /// Starts the Mic, and plays the audio back in (near) real-time.
    private void StartMicListener()
    {
        _audioSource.clip = Microphone.Start("Built-in Microphone", true, 999, FREQUENCY);
        // HACK - Forces the function to wait until the microphone has started, before moving onto the play function.
        while (!(Microphone.GetPosition("Built-in Microphone") > 0)) { }
        _audioSource.Play();
    }

    public float[] pitch = new float[2];
    public float[] decibel = new float[2];

    /// Credits to aldonaletto for the function, http://goo.gl/VGwKt
    /// Analyzes the sound, to get volume and pitch values.
    private void AnalyzeSound()
    {
        for (int c = 0; c < 2; c++)
        {
            // Get all of our samples from the mic.
            _audioSource.GetOutputData(samples, c);

            // Sums squared samples
            float sum = 0;
            for (int i = 0; i < samplecount; i++)
            {
                sum += Mathf.Pow(samples[i], 2);
            }

            // RMS is the square root of the average value of the samples.
            rmsValue = Mathf.Sqrt(sum / samplecount);
            dbValue = 20 * Mathf.Log10(rmsValue / REFVALUE);

            // Clamp it to {clamp} min
            if (dbValue < -clamp)
            {
                dbValue = -clamp;
            }

            // Gets the sound spectrum.
            _audioSource.GetSpectrumData(spectrum, c, FFTWindow.BlackmanHarris);
            float maxV = 0;
            int maxN = 0;

            // Find the highest sample.
            for (int i = 0; i < samplecount; i++)
            {
                if (spectrum[i] > maxV && spectrum[i] > threshold)
                {
                    maxV = spectrum[i];
                    maxN = i; // maxN is the index of max
                }
            }

            // Pass the index to a float variable
            float freqN = maxN;

            // Interpolate index using neighbours
            if (maxN > 0 && maxN < samplecount - 1)
            {
                float dL = spectrum[maxN - 1] / spectrum[maxN];
                float dR = spectrum[maxN + 1] / spectrum[maxN];
                freqN += 0.5f * (dR * dR - dL * dL);
            }
            // Convert index to frequency
            pitchValue = freqN * 24000 / samplecount;

            Pitch[c] = pitchValue;
            Decibel[c] = dbValue * sensivity;
        }

    }
}