﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class TribeManager : MonoBehaviour
{
    public static TribeManager instance;
    public List<TribesMan> tribesMen;
    public TribesMan chief;
    public GameObject[] tribesManPrefab;
    public Transform tribePos;
    public int tribesManCount = 50;
    public Vector2 spawnDist;
    public Vector2 runDist;

    public int minDamage;
    public int maxDamage;

    private List<Vector3> _defPositions;
    private List<Vector3> _runAwayPositions;
    private Vector3 _chiefRunPos;
    private Vector3 _chiefStart;
    public float _progress;

    public float Progress
    {
        get { return _progress; }
        set
        {
            _progress = Mathf.Lerp(0, 1f, value);
        }
    }

    // Use this for initialization
    void OnEnable()
    {
        instance = this;
        tribesMen = new List<TribesMan>();
        _defPositions = new List<Vector3>();
        _runAwayPositions = new List<Vector3>();

        Vector3 worldPos = tribePos.position;
        worldPos.z = 0;

        _chiefStart = chief.transform.position;

        for (int i = 0; i < tribesManCount; i++)
        {
            tribesMen.Add(Instantiate(tribesManPrefab[Random.Range(0, tribesManPrefab.Length)]).GetComponent<TribesMan>());
            _defPositions.Add(worldPos +
                new Vector3(Random.Range(-spawnDist.x, spawnDist.x), Random.Range(-spawnDist.y, spawnDist.y), -7));

            _runAwayPositions.Add(tribesMen[i].transform.position + new Vector3(Random.Range(-runDist.x, runDist.x), -runDist.y, 0));
        }

        _chiefRunPos = tribePos.position + new Vector3(Random.Range(-runDist.x, runDist.x), -runDist.y, 0);
    }

    public void SetState(TribesMan.TribesManState state)
    {
        for (int i = 0; i < tribesMen.Count; i++)
        {
            tribesMen[i].SetState(state);
        }
        if (chief) chief.SetState(state);
    }

    public void ApplyDamage()
    {
        if (tribesMen.Count > 0)
        {
            int damage = (int)Mathf.Lerp(maxDamage, minDamage, _progress);
            for (int i = 0; i < damage && tribesMen.Count > 0; i++)
            {
                tribesMen[0].SetState(TribesMan.TribesManState.Die);
                tribesMen.RemoveAt(0);
                _defPositions.RemoveAt(0);
                _runAwayPositions.RemoveAt(0);
            }
        }
        else if (_progress < 0.5f)
        {
            if (chief) chief.SetState(TribesMan.TribesManState.Die);
        }

        if (chief.CurrentState == TribesMan.TribesManState.Die) SceneManager.LoadScene("GameOver");
    }

    // Update is called once per frame
    void Update()
    {
        if (tribesMen != null)
        {
            if (_progress > 1) _progress = 1;
            for (int i = 0; i < tribesMen.Count; i++)
            {
                tribesMen[i].transform.position = Vector3.Lerp(_defPositions[i], _runAwayPositions[i], _progress);
            }
            if (chief) chief.transform.position = Vector3.Lerp(_chiefStart, _chiefRunPos, _progress);
        }
    }
}
