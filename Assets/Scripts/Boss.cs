﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{
    public AudioSource audioSource;
    public int maxHealth = 5;
    private int healthLeft;

    private float _burstCharge;

    public float minChargeSpeed;
    public float maxChargeSpeed;
    public float delay = 2;

    private float _delayLeft;
    private float currentChargeSpeed;

    public Transform target;
    public Transform start;

    public GameObject _meteorPrefab;

    public Slider _attackSlider;

    private ChantManager cm;

    public int HealthLeft
    {
        get { return healthLeft; }
        set
        {
            healthLeft = value;
            if (healthLeft <= 0) SceneManager.LoadScene("Win");
        }
    }

    // Use this for initialization
    void Start()
    {
        healthLeft = maxHealth;
        currentChargeSpeed = Mathf.Lerp(minChargeSpeed, maxChargeSpeed, Random.Range(0, 1f));
        cm = FindObjectOfType<ChantManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!cm.IsCalibrationFinished()) return;
        if (cm.SelectedChant == ChantManager.ChantType.Ulti) return;
        if (!cm.canStart) return;

        if (_delayLeft > 0)
        {
            _delayLeft -= Time.deltaTime;
            return;
        }

        _burstCharge += currentChargeSpeed * Time.deltaTime;
        if (_burstCharge > 1)
        {
            audioSource.Play();
            currentChargeSpeed = Mathf.Lerp(minChargeSpeed, maxChargeSpeed, Random.Range(0, 1f));
            _delayLeft = delay;
            _burstCharge = 0;
            (Instantiate(_meteorPrefab, start.position, Quaternion.identity) as GameObject).
                GetComponent<Projectile>().target = target;
        }
        _attackSlider.value = _burstCharge;
    }
}
