﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour
{
    public static Intro instance;
    public GameObject intro1;
    public GameObject intro2;
    public GameObject intro3;
    public GameObject chief;
    public GameObject tribe;

    public bool skip = true;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void OnEnable()
    {
        intro3.SetActive(false);
        intro1.SetActive(true);
        intro2.SetActive(false);
        StartCoroutine(IntroRoutine());
    }

    IEnumerator IntroRoutine()
    {
        if (!skip) yield return new WaitForSeconds(20);
        Destroy(intro1);
        intro2.SetActive(true);
        if (!skip) yield return new WaitForSeconds(5);
        Debug.Log("Activate");
        chief.SetActive(true);
        tribe.SetActive(true);
        if (!skip) yield return new WaitForSeconds(1);
        Debug.Log("Activate 2");
        Destroy(intro2);
        yield return new WaitForSeconds(1);
        Debug.Log("Activate 3 ");
        instance = null;
        GetComponent<AudioSource>().Stop();
        isDone = true;
    }

    public bool isDone;
}
