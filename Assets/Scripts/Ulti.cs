﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ulti : MonoBehaviour
{
    public Transform rainTarget;
    public GameObject rainExplosion;
    public GameObject waterBalloonPrefab;
    private List<Transform> _baloons = new List<Transform>();
    private ChantManager _cm;
    private Transform _ballloonPosition;
    // Use this for initialization
    void Awake()
    {
        _cm = FindObjectOfType<ChantManager>();
        _cm.OnSuccess += OnSuccess;
        _cm.OnFail += OnFail;

        _ballloonPosition = FindObjectOfType<TribeManager>().tribePos;
    }

    public void SpawnWaterBalloons(int count)
    {
        Vector3 pos = _ballloonPosition.position;
        pos.z = -8;
        pos.y += 3;
        for (int i = 0; i < count; i++)
        {
            _baloons.Add((Instantiate(waterBalloonPrefab, pos, Quaternion.identity) as GameObject).transform);
        }
    }

    void OnDestroy()
    {
        ChantManager cm = FindObjectOfType<ChantManager>();
        if (cm)
        {
            cm.OnSuccess -= OnSuccess;
            cm.OnFail -= OnFail;
        }
    }

    private void OnFail()
    {
        Debug.Log("Ulti Fail");
        foreach (Transform baloon in _baloons)
        {
            if (baloon) Destroy(baloon.gameObject);
        }
        OnDestroy();
        if (gameObject) Destroy(gameObject);
    }

    private void OnSuccess()
    {
        if (_cm.SelectedChant == ChantManager.ChantType.Ulti)
        {
            Instantiate(rainExplosion, rainTarget.position, Quaternion.identity);
        }
    }

    public Transform GetBalloon()
    {
        Transform item = _baloons[0];
        _baloons.RemoveAt(0);
        return item;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
