﻿using UnityEngine;
using System.Collections;

public class DestroyInSecs : MonoBehaviour
{
    public bool disable = false;
    public float time;

    private float _timeLeft;
	// Use this for initialization
	void OnEnable ()
	{
	    _timeLeft = time;
	}
	
	// Update is called once per frame
	void Update ()
	{
        if (_timeLeft > 0)
        {
            _timeLeft -= Time.deltaTime;
            if (_timeLeft <= 0)
            {
                if (disable) gameObject.SetActive(false);
                else Destroy(gameObject);
            }
        }

	}
}
