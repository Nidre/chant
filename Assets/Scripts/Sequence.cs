﻿using System;
using UnityEngine;
using System.Collections;
using System.ComponentModel;
using System.Xml.Schema;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Sequence : MonoBehaviour
{
    public Chant[] chants;
    public float bpm;
    public float tolerance = 0.1f;

    public string[] chantWords;

    private float _bpmAsTime;
    private float _cumBpm;
    private int _index;
    private int _pattern;
    private int _selectedChant = -1;
    private bool _wordChanted;

    public Text helpText;

    private DetectSyllable _ds;

    // Use this for initialization
    void Start()
    {
        helpText.text = "Select Chant";
        _bpmAsTime = 60f / bpm;
        _ds = FindObjectOfType<DetectSyllable>();
        _ds.OnShortSyllable += OnShortSyllable;
    }

    private void OnShortSyllable(DetectSyllable.Syllable syllable)
    {
        if (_selectedChant == -1)
        {
            Debug.Log(syllable + " - activated");
            //_selectedChant = syllable.index;
            _index = 0;
        }
        else
        {
            _wordChanted = true;
            if (Mathf.Abs(_bpmAsTime - _cumBpm) < tolerance)
            {
                if (_index - 1 < chants[_selectedChant].pattern.Length)
                {
                    if (_index > 0 && chants[_selectedChant].pattern[_index - 1]) Debug.Log("Helal");
                    else Debug.Log("Ov Shit");
                }
                else Debug.Log("Ov Shit");
            }
            else Debug.Log("Ov Shit");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_selectedChant == -1) return;
        _cumBpm += Time.deltaTime;
        if (_cumBpm > _bpmAsTime)
        {
            _cumBpm = 0;
            _index++;
            _wordChanted = false;
            if (_index > chants[_selectedChant].pattern.Length + 1)
            {
                _index = 0;
                _selectedChant = -1;
            }
            else
            {
                SetChantWord();
            }
        }
    }

    private void SetChantWord()
    {   
        int index = _index - 1;
        if (index < chants[_selectedChant].pattern.Length)
        {
            if (index >= 0 && chants[_selectedChant].pattern[index])
            {
                string prevWord = helpText.text;
                while (prevWord == helpText.text)
                {
                    helpText.text = chantWords[Random.Range(0, chantWords.Length)];
                }
            }
            else helpText.text = "Wait!";
        }
        else
        {
            helpText.text = "Select Chant";
        }
        Debug.Log(helpText.text);
    }

    [Serializable]
    public class Chant
    {
        public bool[] pattern;
    }
}
