﻿using UnityEngine;
using System.Collections;

public class AudioInputTest : MonoBehaviour
{
    private AudioSource _audioSource;

    // Use this for initialization
    void Start ()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = Microphone.Start("Built-in Microphone", true, 1, 44100);
        _audioSource.loop = true;
        _audioSource.mute = true;
        _audioSource.Play();
    }

    public string state = "";
    public int index;
    public float[] data;

	// Update is called once per frame
	void Update ()
	{
	    data = AudioSpectrum.Instance.Levels;

        int highestIndex = 0;
	    for (int i = 1; i < AudioSpectrum.Instance.Levels.Length; i++)
	    {
            if (AudioSpectrum.Instance.Levels[i] > AudioSpectrum.Instance.Levels[highestIndex])
            {
                highestIndex = i;
            }
        }
	    index = highestIndex;

        if (highestIndex <= 1) state = "Bas";
	    else state = "Tiz";

    }

}
