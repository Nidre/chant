﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ChantManager : MonoBehaviour
{
    public enum ChantType
    {
        None,
        Charge,
        Protection,
        Ulti
    }

    public AudioClip _chantSound;
    public AudioClip _defenseSound;
    public AudioClip _idleSound;

    public AudioSource audioSource;

    public GameObject rain;
    public GameObject waterBombPrefab;
    public Transform waterBombPosition;
    private Transform waterBomboTransform;
    public Transform balloonTarget;
    public float ultiChargeRate;
    public float ultiCharge;
    public Image skyProgress;
    public Image ultiImage;
    public GameObject ultiPrefab;
    public Text[] texts;

    public float ultiUnlockDelay = 5;
    public float ultiDechargeRate = 1;
    private float ultiLockLeft;

    public Action OnFail;
    public Action OnSuccess;

    public Chant chargeChant;
    public Chant ultiChant;
    public string[] chantWords;
    public float decibelChargeRate = 1;
    public float decibelDechargeRate = 5;
    public float shieldDuration = 4;

    private Chant _activeChant;

    public float tolerance;
    public Text shieldText;

    public float bpm;
    private float _bpmAsTime;
    private DetectSyllable _ds;
    private ChantType _selectedChant = ChantType.None;
    private bool _isWordChanted;
    private float _timePassed;
    private float _shieldDurationLeft;

    private int _chantStep;

    private bool _willEnd;

    public float maxDecibel;
    public float minDecibel;

    public float calibrationTime = 5;
    private float _calibrationTimeLeft;
    private int _calibrationStep;

    public float minShieldTime = 1;
    private float _minShieldTime;

    private Transform _balloon;
    public TribeManager tm;

    public GameObject calibrationSilence;
    public GameObject calibrationShout;


    public Image silenceFill;
    public Image shoutFill;

    public GameObject introGo;

    public bool canStart = false;

    public ChantType SelectedChant
    {
        get { return _selectedChant; }
    }

    // Use this for initialization
    void Start()
    {
        calibrationShout.SetActive(false);
        _bpmAsTime = 60f / bpm;
        _ds = FindObjectOfType<DetectSyllable>();
        _ds.OnShortSyllable += OnShortSyllable;
    }

    private void OnShortSyllable(DetectSyllable.Syllable syllable)
    {
        if (!IsCalibrationFinished()) return;
        if (!tm) return;
        if (!canStart) return;

        Debug.Log("Word OK");

        if (SelectedChant == ChantType.None)
        {
            if (skyProgress.fillAmount < 1 && syllable.chantType == ChantType.Ulti) return;
            _isWordChanted = false;
            _chantStep = 0;
            _selectedChant = syllable.chantType;
            switch (SelectedChant)
            {
                case ChantType.Charge:
                    _activeChant = chargeChant;
                    tm.SetState(TribesMan.TribesManState.Chant);
                    audioSource.clip = _chantSound;
                    audioSource.Play();
                    break;
                case ChantType.Protection:
                    audioSource.clip = _defenseSound;
                    audioSource.Play();
                    _activeChant = null;
                    _shieldDurationLeft = shieldDuration;
                    shieldText.text = "";
                    _minShieldTime = minShieldTime;
                    tm.SetState(TribesMan.TribesManState.Sway);
                    break;
                case ChantType.Ulti:
                    audioSource.clip = _chantSound;
                    _activeChant = ultiChant;
                    tm.SetState(TribesMan.TribesManState.Chant);
                    audioSource.Play();
                    break;
            }
            Debug.Log(syllable + " - activated");
        }
        else
        {
            if (_isWordChanted)
            {
                Debug.Log("Fail");
                if (OnFail != null) OnFail();
            }
            else
            {
                switch (SelectedChant)
                {
                    case ChantType.Charge:
                        ChantInput();
                        break;
                    case ChantType.Protection:
                        break;
                    case ChantType.Ulti:
                        ChantInput();
                        break;
                }
            }
        }
    }

    public bool IsCalibrationFinished()
    {
        return _calibrationStep >= 2;
    }

    public void ChantInput()
    {
        _isWordChanted = true;
        if (_bpmAsTime - _timePassed < tolerance)
        {
            if (_chantStep - 1 < _activeChant.pattern.Length)
            {
                if (_chantStep > 0 && _activeChant.pattern[_chantStep - 1])
                {
                    CorrectTiming();
                    return;
                }
            }
        }

        Debug.Log("Fail");
        if (_selectedChant == ChantType.Ulti)
        {
            _selectedChant = ChantType.None;
            _chantStep = 1;
            _activeChant = null;
        }
        if (OnFail != null)
        {
            OnFail();
        }
    }

    private void CorrectTiming()
    {
        if (OnSuccess != null) OnSuccess();
        Debug.Log("Helal");
        ultiCharge += ultiChargeRate;
        if (ultiCharge > 1) ultiCharge = 1;
        skyProgress.fillAmount = Mathf.Lerp(0.3f, 1, ultiCharge);

        if (_selectedChant == ChantType.Ulti)
        {
            waterBomboTransform = (Instantiate(waterBombPrefab,
                waterBombPosition.position,
                Quaternion.identity) as GameObject).transform;
            waterBomboTransform.GetComponent<Projectile>().target = balloonTarget;
        }

        if (_balloon)
        {
            Destroy(_balloon.gameObject);
            _balloon = null;
        }
    }

    public bool maxLock = false;


    void LateUpdate()
    {
        if (SelectedChant != ChantType.Ulti)
        {
            skyProgress.fillAmount = Mathf.Lerp(skyProgress.fillAmount, Mathf.Lerp(0.3f, 1, ultiCharge), Time.deltaTime);
            if (skyProgress.fillAmount >= 1)
            {
                skyProgress.fillAmount = 1;
                ultiLockLeft = ultiUnlockDelay;
                ultiImage.gameObject.SetActive(true);

                _chantStep = 1;
                _activeChant = null;
                _willEnd = true;
                _selectedChant = ChantType.Ulti;
                _activeChant = ultiChant;
                tm.SetState(TribesMan.TribesManState.Chant);
                _timePassed = 0;
                rain.SetActive(true);
            }
            ultiCharge -= Time.deltaTime * ultiDechargeRate;
            if (ultiCharge < 0) ultiCharge = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_calibrationStep < 2)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                _calibrationTimeLeft = calibrationTime;
                Debug.Log("Space: " + _calibrationStep);
            }
            if (_calibrationTimeLeft > 0)
            {
                _calibrationTimeLeft -= Time.deltaTime;
                if (_calibrationStep == 0) silenceFill.fillAmount = 1f - (_calibrationTimeLeft / calibrationTime);
                else shoutFill.fillAmount = 1f - (_calibrationTimeLeft / calibrationTime);
                if (_calibrationStep == 0)
                {
                    if (MicHandle.Instance.Decibel[0] < minDecibel) minDecibel = MicHandle.Instance.Decibel[0];
                    if (_calibrationTimeLeft <= 0)
                    {
                        _calibrationStep++;
                        calibrationShout.SetActive(true);
                        calibrationSilence.SetActive(false);
                        Debug.Log("minDecibel Calibration Ended");
                    }
                }
                else if (_calibrationStep == 1)
                {
                    if (MicHandle.Instance.Decibel[0] > maxDecibel) maxDecibel = MicHandle.Instance.Decibel[0];
                    if (_calibrationTimeLeft <= 0)
                    {
                        _calibrationStep++;
                        calibrationShout.SetActive(false);
                        calibrationSilence.SetActive(false);
                        introGo.SetActive(true);
                        Debug.Log("maxDecibel Calibration Ended");
                    }
                }
            }
        }

        if (Time.realtimeSinceStartup < 25) return;
        if (Intro.instance != null) return;

        canStart = true;

        switch (SelectedChant)
        {
            case ChantType.Charge:
            case ChantType.Ulti:
                if (_activeChant == null) return;
                _timePassed += Time.deltaTime;
                if (Input.GetMouseButtonDown(0))
                {
                    CorrectTiming();
                }

                if (_timePassed > _bpmAsTime)
                {
                    _timePassed = 0;
                    _chantStep++;
                    _isWordChanted = false;
                    if (_chantStep > _activeChant.pattern.Length)
                    {
                        if (_selectedChant == ChantType.Ulti)
                        {
                            ultiCharge = 0;
                            skyProgress.fillAmount = 0;
                        }
                        _chantStep = 1;
                        _activeChant = null;
                        _willEnd = true;
                        _selectedChant = ChantType.None;
                        tm.SetState(TribesMan.TribesManState.Idle);
                        rain.SetActive(false);
                        audioSource.clip = _idleSound;
                        Debug.Log("Chant End");
                    }
                    SetChantWord();
                }

                break;
            case ChantType.Protection:
                if (_activeChant != null) return;
                if (_shieldDurationLeft > 0)
                {
                    _minShieldTime -= Time.deltaTime;
                    _shieldDurationLeft -= Time.deltaTime;
                    if (_shieldDurationLeft <= 0)
                    {
                        _chantStep = 1;
                        _activeChant = null;
                        _willEnd = true;
                        SetChantWord();
                        _isWordChanted = false;
                        tm.SetState(TribesMan.TribesManState.Idle);
                        audioSource.clip = _idleSound;
                        Debug.Log("Shield End");
                    }
                }

                float diff = Mathf.Abs(maxDecibel - minDecibel);
                float decibelLevel = (MicHandle.Instance.Decibel[0] - minDecibel) / diff;
                if (Input.GetKey(KeyCode.Space)) decibelLevel = 1;
                if (_minShieldTime <= 0 && (decibelLevel < 0.4f || _shieldDurationLeft <= 0) && TribeManager.instance.Progress > 0)
                {
                    TribeManager.instance.Progress -= decibelDechargeRate;

                    if (TribeManager.instance.Progress <= 0)
                    {
                        TribeManager.instance.Progress = 0;
                        if (_willEnd)
                        {
                            _selectedChant = ChantType.None;
                            _willEnd = false;
                            Debug.Log("Shield End Charge");
                        }
                    }
                }
                else TribeManager.instance.Progress += decibelChargeRate;
                shieldText.text = decibelLevel.ToString();
                break;
        }

    }

    private void SetChantWord()
    {
        if (_activeChant == null)
        {
            return;
        }
        int index = _chantStep - 1;
        if (index < _activeChant.pattern.Length)
        {
            if (index >= 0 && _activeChant.pattern[index])
            {
                string prevWord = texts[0].text;
                string newWord = chantWords[Random.Range(0, chantWords.Length)];
                while (prevWord == newWord)
                {
                    newWord = chantWords[Random.Range(0, chantWords.Length)];
                }
                foreach (Text helpText in texts)
                {
                    helpText.gameObject.SetActive(false);
                    helpText.text = newWord;
                    helpText.gameObject.SetActive(true);
                }
            }
            else
            {
                foreach (Text helpText in texts)
                {
                    helpText.gameObject.SetActive(false);
                    helpText.text = "Wait!";
                    helpText.gameObject.SetActive(true);
                }
            }
        }
        Debug.Log(texts[0]);
    }

    [Serializable]
    public class Chant
    {
        public bool[] pattern;
    }
}
