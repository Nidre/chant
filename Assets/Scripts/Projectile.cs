﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{

    public float speed;

    private Transform _transform;

    public Transform target;
    public Vector3 minScale;
    public Vector3 normalScale;
    public GameObject explosion;
    public bool managed = false;
    private Vector3 _startPos;

    public float arcHeight = 3;
    public bool ark = true;
    public bool meteor = true;
    public bool damageBoss = false;

    public AudioSource audioSource;

    private float _progress;

    public float Progress
    {
        get { return _progress; }
        set
        {
            _progress = value;
        }
    }

    // Use this for initialization
    void Awake()
    {
        _transform = transform;
        _startPos = _transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!target) return;
        if (_progress < 1)
        {
            if (!managed) _progress += Time.deltaTime * speed;
            _transform.localScale = Vector3.Lerp(minScale, normalScale, _progress);
            _transform.Rotate(0, 0, 5);
            if (ark) Arc(_progress);
            else _transform.position = Vector3.Lerp(_startPos, target.position, _progress);
            if (_progress >= 1)
            {
                _progress = -1;
                Instantiate(explosion, _transform.position, Quaternion.identity);
                if (meteor) TribeManager.instance.ApplyDamage();
                if (damageBoss) FindObjectOfType<Boss>().HealthLeft--;
                if (audioSource) audioSource.Play();
                Destroy(gameObject);
            }
        }
    }

    public void Explode()
    {
        Instantiate(explosion, _transform.position, Quaternion.identity);
    }

    void Arc(float progress)
    {
        Vector3 distance = target.position - _startPos;
        Vector3 jumpVector = Quaternion.Euler(0, 90, 0) * distance * Mathf.Sin(progress * Mathf.PI) * arcHeight;
        _transform.position = _startPos + (distance * progress) + jumpVector;
    }
}
