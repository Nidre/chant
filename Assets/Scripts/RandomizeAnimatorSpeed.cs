﻿using UnityEngine;
using System.Collections;

public class RandomizeAnimatorSpeed : MonoBehaviour
{

    public float minSpeed = 0.7f;
    public float maxSpeed = 1.2f;

    // Use this for initialization
    void OnEnable()
    {
        GetComponent<Animator>().speed = Random.Range(minSpeed, maxSpeed);
    }
}
